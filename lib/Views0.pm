#File Views.pm
package Views;

my %css = ('species'  => qq{font-style: italic;\n},
           'odd'      => qq{background-color: #FFA500;\n}, #oranges
           'oddodd'   => qq{background-color: #FFDAB9;\n}, #peachpuff
           'oddeven'  => qq{background-color: #e8bcad;\n},
           'even'     => qq{background-color: #5CDC4F;\n}, #greens
           'evenodd'  => qq{background-color: #CBFFC6;\n},
           'eveneven' => qq{background-color: #ABCFA7;\n},
           'match_on' => qq{background-color: #BD43EF;\n            font-weight: bolder;\n}, #violet
          );

my $ncbi_nt = 'http://www.ncbi.nlm.nih.gov/nuccore/';
my $ncbi_aa = 'http://www.ncbi.nlm.nih.gov/protein/';
my $uniprot = 'http://www.uniprot.org/uniprot/';
my $pdb     = 'http://www.pdb.org/pdb/explore/explore.do?structureId=';

my %species = ( 'alfalfa'    => '',
                'baboon'     => '',
                'barley'     => '',
                'bovine'     => '',
                'cat'        => '',
                'chicken'    => '',
                'chimp'      => '',
                'chimpanzee' => '',
                'cow'        => '',
                'dog'        => '',
                'duck'       => '',
                'goat'       => '',
                'gorilla'    => '',
               #'guina pig'  => '', " Works as normal species name
                'hamster'    => '',
                'hamsters'   => '',
                'hiv-1'      => '',
                'hiv1'       => '',
                'hiv'        => '',
                'horse'      => '',
                'hsv1'       => '',
                'human'      => '', #All in lowercase
                'maize'      => '',
                'mouse'      => '',
                'oat'        => '',
                'pea'        => '',
                'pig'        => '',
                'porcine'    => '',
                'potato'     => '',
                'pumpkin'    => '',
                'rabbit'     => '',
                'rat'        => '',
                'rats'       => '',
                'rice'       => '',
                'salamander' => '',
                'salmon'     => '',
                'sheep'      => '',
                'soybean'    => '',
                'soybeans'   => '',
                'spinach'    => '',
                'sunflower'  => '',
                'tobacco'    => '',
                'wheat'      => '',
                'yeast'      => '',
                'zebrafish'  => '',
              );
my %nonSpecies = ( 'mRNA'             => '',
                   'cDNA'             => '',
                   'DNA'              => '',
                   'carboxypeptidase' => '',
                   'chloroplast'      => '',
                   'endosymbiont'     => '',
                   'gene'             => '',
                   'phage'            => '',
                   'prophage'         => '',
                   'retrotransposon'  => '',
                   'sequence'         => '',
                   'superoxide'       => '',
                   'synthetic'        => '',
                   'transposon'       => '',
                 );

#FIXME: fake match if 1st word is a single letter !
my %fakeMatch = ('acetylcholinesterase T'                => '',
                 'Arsenite-oxidising bacterium'          => '',
                 'Bacterial enrichment'                  => '',
                 'Boar'                                  => '',
                 'C-terminal'                            => '',
                 'Cu-Zn'                                 => '',
                 'chloroplast'                           => '',
                 'Cloning vector'                        => '',
                 'Control vector'                        => '',
                 'cysteine proteinase'                   => '',
                 'delta proteobacterium'                 => '',
                 'dihydrofolate reductase-thymidylate'   => '',
                 'Donor vector'                          => '',
                 'endosymbiont'                          => '',
                 'Eukaryotic synthetic'                  => '',
                 'Expression vector'                     => '',
                 'Gamma proteobacterium'                 => '',
                 'Genomic sequence'                      => '',
                 'glycogen synthase'                     => '',
                 'high-potential iron'                   => '',
                 'Integrating YFP'                       => '',
                 'Integration vector'                    => '',
                 'Landing site'                          => '',
                 'Ligation-independent promoter-cloning' => '',
                 'Luciferase'                            => '',
                 'Mammalian expression'                  => '',
                 'Marine gamma'                          => '',
                 'Mariner mini-transposon'               => '',
                 'Messenger RNA'                         => '',
                 'N-terminal'                            => '',
                 'nonselective-type endothelin'          => '',
                 'PCR cloning'                           => '',
                 'PCR template'                          => '',
                 'Plasmid'                               => '',
                 'Plastid transformation'                => '',
                 'Prokaryote enrichment'                 => '',
                 'Promoter probe'                        => '',
                 'pyruvate carboxylase'                  => '',
                 'Recombinase expression'                => '',
                 'Reporter vector'                       => '',
                 'retrotransposon'                       => '',
                 'self-incompatibility'                  => '',
                 'Sender Vector'                         => '',
                 'Shuttle vector'                        => '',
                 'Suicide vector'                        => '',
                 'Synthetic barnase'                     => '',
                 'Synthetic construct'                   => '',
                 'Tetracycline-inducible RNAi'           => '',
                 'transcription factor'                  => '',
                 'transposon'                            => '',
                 'T-DNA vector'                          => '',
                 'Uncultured bacterium'                  => '',
                 'Uncultured marine'                     => '',
                 'Uncultured mollusc'                    => '',
                 'Uncultured microorganism'              => '',
                 'Uncultured organism'                   => '',
                 'Uncultured prokaryote'                 => '',
                 'Uncultured proteobacterium'            => '',
                 'UAS-less'                              => '',
                 'virus vector'                          => '',
                 'YAC construction'                      => '',
                );

my %Strain = ('biogroup'        => '',
              'biovar'          => '',
              'breed'           => '',
              'bv.'             => '',
              'cell line'       => '', #Cricetulus griseus cell line CHO-K1
              'cf.'             => '',
              'class'           => '',
              'clone'           => '',
              'colony'          => '',
              'cultivar'        => '',
              'cultivar-group'  => '', #Oryza sativa (indica cultivar-group)
              'cv.'             => '',
              'cv'              => '',
              'ecotype'         => '', #Arabidopsis thaliana ecotype Landsberg erecta
              'f.'              => '', #Volvox carteri f. nagariensis = forma
              'forma'           => '',
              'genotype'        => '',
              'group'           => '', #Oryza sativa Japonica Group
              'haplogroup'      => '',
              'haplotype'       => '',
              'isolate'         => '',
              'patens'          => '',
              'pestoides'       => '',
              'phage'           => '',
              'plasmid'         => '',
              'proteobacterium' => '',
              'pv.'             => '',
              'serotype'        => '',
              'serovar'         => '',
              'sp.'             => '',
              'ssp.'            => '',
              'str.'            => '',
              'strain'          => '',
              'stylar'          => '',
              'subclone'        => '',
              'subsp.'          => '',
              'substr.'         => '',
              'transposon'      => '',
              'type'            => '',
              'vaccine'         => '',
              'var.'            => '',
              'vector'          => '',
              'voucher'         => '',
              'wild-type'       => '',
              'x'               => '',
);

my %large_names = ( 'MNPV'   => '',
                    'NPV'    => '', #nucleopolyhedrovirus
                    'NPV-A'  => '',
                    'NPV-B'  => '',
                    'SNPV'   => '',
                    'viral'  => '',
                    'virus'  => '', #FIXME: Highlight also (... virus \d\w*)
);

#FIXME How to deal with Megavirus chiliensis / Megavirus lba because genera ends with virus!

my %longer_species_name = ( 'afer'              => '', #Orycteropus afer afer
                            'altaica'           => '', #Panthera tigris altaica
                            'alvus'             => '', #Candidatus Methanomethylophilus alvus
                            'andium'            => '', #Anas flavirostris andium
                            'australis'         => '', #Struthio camelus australis
                            'bairdii'           => '', #Peromyscus maniculatus bairdii
                            'bellii'            => '', #Chrysemys picta bellii
                            'birmanicus'        => '', #Bos javanicus birmanicus
                            'bison'             => '', #Bison bison bison
                            'boliviensis'       => '', #Saimiri boliviensis boliviensis
                            'bretschneideri'    => '', #Pyrus x bretschneideri
                            'brucei'            => '', #Trypanosoma brucei brucei
                            'calamorum'         => '', #Microtus fortis calamorum
                            'canadensis'        => '', #Aquila chrysaetos canadensis
                            'chabaudi'          => '', #Plasmodium chabaudi chabaudi
                            'cornix'            => '', #Corvus cornix cornix
                            'corporis'          => '', #Pediculus humanus corporis
                            'coturnix'          => '', #Coturnix coturnix japonica
                            'crecca'            => '', #Anas crecca crecca
                            'cultivar'          => '', #Salix hybrid cultivar
                            'dingo'             => '', #Canis lupus dingo
                            'divergens'         => '', #Odobenus rosmarus divergens
                            'domestica'         => '',
                            'domesticus'        => '', #Anser cygnoides domesticus
                            'dorsalis'          => '', #Seriola lalandi dorsalis
                            'familiaris'        => '',
                            'flavirostris'      => '', #Anas flavirostris flavirostris
                            'fretensis'         => '', #Anas versicolor fretensis
                            'furo'              => '', #Mustela putorius furo
                            'gallus'            => '',
                            'gambiense DAL972'  => '', #Trypanosoma brucei gambiense DAL972
                            'gibbericeps'       => '', #Balearica regulorum gibbericeps
                            'gorilla'           => '', #Gorilla gorilla gorilla
                            'ibex'              => '', #Capra ibex ibex
                            'imitator'          => '', #Cebus capucinus imitator
                            'japonica group'    => '', #Oryza sativa Japonica Group
                            'latirostris'       => '', #Trichechus manatus latirostris
                            'marmota'           => '', #Marmota marmota marmota
                            'mexicana'          => '', #Leishmania mexicana mexicana
                            'musimon'           => '', #Ovis aries musimon
                            'mutus'             => '', #Bos grunniens mutus
                            'neanderthalensis'  => '', #Homo sapiens neanderthalensis
                            'orientalis'        => '', #Panthera pardus orientalis
                            'palliatus'         => '', #Colobus angolensis palliatus
                            'persica'           => '', #Panthera leo persica
                            'pseudoobscura'     => '', #Drosophila pseudoobscura pseudoobscura
                            'quinquefasciatus'  => '', #Culex pipiens quinquefasciatus
                            'Red Jungle fowl'   => '', #Gallus gallus breed Red Jungle fowl
                            'rubra'             => '', #Varecia variegata rubra
                            'saxicolor'         => '', #Panthera pardus saxicolor
                            'scammoni'          => '', #Balaenoptera acutorostrata scammoni
                            'septentrionalium'  => '', #Anas cyanoptera septentrionalium
                            'simum'             => '', #Ceratotherium simum simum
                            'Sprague-Dawley'    => '', #Rattus norvegicus Sprague-Dawley
                            'sp.'               => '',
                            'sumatrae'          => '', #Panthera tigris sumatrae
                            'SY8'               => '', #Achromobacter arsenitoxydans SY8
                            'texanus'           => '', #Odocoileus virginianus texanus
                            'ubiquis'           => '', #Candidatus Fonsibacter ubiquis
                            'variegata'         => '', #Varecia variegata variegata
                            'versicolor'        => '', #Anas versicolor versicolor
                            'verus'             => '', #Pan troglodytes verus
                            'yoelii'            => '',
                            'x'                 => '',
);

#FIXME: Xenopus (Silurana) tropicalis ?

sub Html {
    my ($file) = @_;
    return 0  if ( $file !~ /\.cdsP$/ || -z $file || !-r $file );

    open (my $CDSP,    '<', "$file")      or die "\tCannot open '$file'\n";
    open (my $CDSHTML, '>', "$file.html") or die "\tCannot create in '$file.html'\n";
    print {$CDSHTML} htmlHeader($file);
    {
        my $isOdd    = 'odd';
        my $isSubOdd = 'odd';
        CDS_FILE:
        while(<$CDSP>){
            chomp();
            next CDS_FILE if ( /^$/ );
            s{>}{&gt;}g;
            s{<}{&lt;}g;
            s{& }{&amp; }g;

            if ( m/ MATCHES_ON (.*)$/) {
                my $match = $1 || '';
                $match    =~ s{  +}{ }g;
                $match    =~ s{\t+}{ }g;
                my $rest  = '';
                #if @desc null ?

                if ( $match =~ /^$fakeMatch{$match}/i || $match =~ /^\w+ gene cluster/ ){
                    $rest = $match;
                }
                elsif ( $match =~ /^(.+?(v|V)ir(us|al))(.*)$/ && $2 !~ /^ resistance/ && $2 !~ /^ oncogene/ ){
                    $match = $1;
                    $rest  = $2 ||'';
                }
                else {
                    my $pred = '';
                    my @desc = split(' ', $match);
                    # TPA is Third Party Annotation; TPA; TPA:REASSEMBLY.
                    if ( $desc[0] =~ /PREDICTED:?/ || $desc[0] eq /Synthetic/  || $desc[0] eq /Uncultured/i || $desc[0] =~ /TSA:?/ || $desc[0] =~ /TPA:?/ || $desc[0] =~ /TPA_inf:?/ || $desc[0] =~ /TPA_asm:?/ || $desc[0] =~ /UNVERIFIED:?/ ){
                        $pred = $1.' ';
                        shift(@desc);
                    }
                    if ( $desc[0] =~ /^(\[?)([a-zA-Z_\.\-]+1?)$/ ){ #1? for HIV-1
                        $pred .= $1 || '';
                        $match = $2 || $1;
                        shift(@desc);
                        if ( $match =~ /\..+$/ || exists($species{lc($match)}) ){
                            $rest = join(' ', @desc);
                            undef @desc;
                        }
                    }
                    my $cross_sp = 0;
                    if ( $desc[0] =~ /^([A-Za-z][\w\.\-]+[;,]?)$/ ){
                        my $spe = $1;
                        $cross_sp = 1 if ( $spe eq 'x' || $spe eq 'lupus' || $spe eq 'gallus' || $spe eq 'gorilla'  || $spe eq 'pseudoobscura' );
                        if ( exists($nonSpecies{$spe}) ){
                            $rest = join(' ', @desc);
                            undef @desc;
                        }
                        else {
                            $match .= ' '.$spe;
                            shift(@desc);
                        }
                    }
                    my $strain = 0;
                    if ( $cross_sp && $desc[0] =~ /^([\w\.\-]+)$/ ){
                        my $spe = $1;
                        #FIXME: Use hash 'longer_species_name' instead of this
                        $spe    = ''  if ( ($match =~ /lupus$/ && $spe ne 'familiaris') || ($match =~ /gallus$/ && $spe ne 'gallus') || ($match =~ /gorilla$/ && $spe ne 'gorilla') );
                        $strain = 1  if ( exists($Strain{$spe}) );
                        $match .= ' '.$spe;
                        shift(@desc);
                    }
                    if ( $strain && $desc[0] =~ /^([\w\.\-]+)$/ ){
                        $match .= ' '.$1;
                        shift(@desc);
                    }

                    $rest = join(' ', @desc) || '';
                    undef @desc;
                    if ( $match =~ /,$/ ){
                        $match =~ s{,$}{};
                        $rest = ', '.$rest;
                    }
                    elsif ( $match =~ /;$/ ){
                        $match =~ s{;$}{};
                        $rest = '; '.$rest;
                    }
                    $match = '&nbsp;' if ( $match eq '' );
                }

                s{MATCHES_ON .*$}{<span class='match_on'>MATCHES_ON $pred<span class='species'>$match</span></span>$rest};
                # , en fin de match
                #need to generalize s{MATCHES_ON \Q$Match\E}{...}
                #problem with "MATCHES_ON H.giganteus (Hpga1)" because second match has parentheses => need to manage match per match and not line type then matches inside in _G_X76876 _S_ CAA54203
            }

            if ( m/^&gt;.*MATCHES_ON / ){
                s{^&gt;([^ ]*)_G_(\w+(\.\d+)?) }{&gt;$1_G_<a href='$ncbi_nt$2' target='_blank'>$2</a> } if ( !m/_G_My_Seq / );
                if ( m/ _S_ (\w+(\.\d+)?) / && ! m/ _S_ My_own_seq / ){
                    my $blastHit = $1;
                    if ( $blastHit =~ /^[A-Z][A-Z0-9]{5}$/ ){
                        s{ _S_ (\w+) }{ _S_ <a href='$uniprot$1' target='_blank'>$1</a> };
                    }
                    elsif ( $blastHit =~ /^\d[a-z0-9]{3}$/i ){
                        s{ _S_ (\w+) }{ _S_ <a href='$pdb$1' target='_blank'>$1</a> };
                    }
                    else {
                        s{ _S_ (\w+(\.\d+)?) }{ _S_ <a href='$ncbi_aa$1' target='_blank'>$1</a> };
                    }
                }

                print {$CDSHTML} "<span class='$isOdd$isSubOdd'>", $_, "\n";
                $isSubOdd = $isSubOdd eq 'odd' ? 'even' : 'odd';
            }
            elsif ( m/^&gt;.*_G_revtrans / ){
                print {$CDSHTML} "<span class='$isOdd$isSubOdd'>", $_, "\n";
                $isSubOdd = $isSubOdd eq 'odd' ? 'even' : 'odd';
            }
            elsif ( m/^&gt;.*/ && !m/MATCHES_ON / ){
                print {$CDSHTML} "<span class='$isOdd'>", $_, "\n";
                $isOdd    = $isOdd eq 'odd' ? 'even' : 'odd';
                $isSubOdd = 'odd';
            }
            else {
                print {$CDSHTML} $_, "</span>\n";
            }
        }
    }
    print {$CDSHTML} &htmlFooter();
    close $CDSHTML;
    close $CDSP;
    return 1;
}


sub htmlHeader {
    my ($jobName) = @_;
    $jobName =~ s{^.*?([^\/]+?)\.cdsP$}{$1};

    my $css = '';
    for my $style ( keys(%css) ){
        $css .= "        .$style {
            $css{$style}        }\n";
    }

    return "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"en\">
<head>
    <title>ProtoGene xHTML/CSS output for $jobName</title>
    <meta http-equiv='content-type' content='text/html;charset=UTF-8' />
    <meta http-equiv='Content-Style-Type' content='text/css' />
    <style type='text/css'>
$css    </style>
</head>

<body>
    <div align='center'><font color='red'>Beta</font> CDS output</div>
    <pre>
";
}

sub htmlFooter {
    return "    </pre>
</body>
</html>
";
}

1;

