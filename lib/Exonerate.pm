#File Exonerate.pm

package Exonerate;


sub parser{
    my ($infile, $debug) = @_;
    my %pos_aln;
    my %pos_boj;

    open(my $MAP, '<', "$infile")  if ( -e "$infile" )  or die "$!\n\n";
    my $flag = 0;
    my ($Match, $Genom, $Query) = ('', '', '');
    my $margeL = 0;
    my (@cdsUp);
    EXONERATE_RES:
    while(<$MAP>){
        if ( $flag==1 && $margeL==0 && $_ =~ /^( +\d+ : )[\w\-\{\} \>\<\*]+ : +\d+$/ ){ #\* for stop codon management
            $margeL = length($1);
        }

        if ( $flag==0 && $_ =~ /^\s+Target range: \d+ -> \d+/ ){
            $flag = 1;
        }
        elsif ( $_ =~ /^vulgar:/ ){
            $flag = 8;
        }
        elsif ( $_ =~ /^\# --- END OF GFF DUMP ---/ ){
            last EXONERATE_RES;
        }
        elsif ( $flag==1 && $_ =~ /^ +\d+ : ([\w\-\{\} \>\<\*]+) : +\d+/ ){
            $Query .= $1;
            $flag++;
        }
        elsif ( $flag==2 && $_ =~ /^ +[\|\+\-\{\}\dbp\.\!\:\#]+/ ){
            $_     =~ s/^.{$margeL}//;
            $Match .= $_;
            chomp($Match);
            $flag++;
        }
        elsif ( $flag==3 && $_ =~ /^ +[\w\+\-\{\}\*\#]+/ ){
            $flag++;
        }
        elsif ( $flag==4 && $_ =~ /^ +\d+ : ([\w\-\{\}\.]+) : +\d+/ ){
            $Genom  .= $1;
            $flag    = 1;
        }
        elsif ( $flag==8 && $_ =~ /exonerate:protein2[\w\:]+\tsimilarity\t\d+\t/ ){
            my $similarity_query = $_;
            chomp($similarity_query);
            $similarity_query    =~ s{^[^;]+?; *}{};
            $similarity_query    =~ s{^Target [^;]+; *}{};
            while( $similarity_query =~ m/^ ?Align (\d+) / ){
                push(@cdsUp, $1);
                $similarity_query =~ s/^ ?Align \d+ \d+ \d+ *;?//;
            }
            $flag = 9;
        }
    }
    close $MAP;
    print {*STDERR} "[@cdsUp]\n"  if ( $debug );


    my $drap     = 0;
    my ($locus, $lieu) = (0, 0);
    my $frameIntron = 0;
    my $boj      = '';
    my $pep_pos  = '';
    my $accolade = 0;
    MATCHES:
    for(my $i=0; $i<length($Match); $i++){
        my $matched_symbol = substr($Match, $i, 1);
        if ( $drap==3 && $matched_symbol =~ /^[\+\-]$/ ){
            $drap  = 0;
            $locus = 0;
            print {*STDERR} $matched_symbol, substr($Genom, $i, 1), "\t", substr($Query, $i, 1), "\n" if ($debug);
        }
        elsif ( $matched_symbol =~ /^[\+\-]$/ ){
            $drap++;
            print {*STDERR} $matched_symbol, substr($Genom, $i, 1), "\t", substr($Query, $i, 1), "\n" if ($debug);
        }
#       elsif ( $matched_symbol =~ /[ \|\.\!\:]/ && substr($Genom,$i,1) !~ /[\{\}\.a-z\-]/ ){
        elsif ( $matched_symbol =~ /[ \|\.\!\:\#]/ && substr($Genom, $i, 1) !~ /[\{\}\.a-z]/ ){
            my $peptide_pos = '';
            $peptide_pos    = $cdsUp[0]+$lieu if ( substr($Query, $i, 1) =~ /[A-Z\*]/ );
            $pep_pos        = $cdsUp[0]+$lieu if ( substr($Query, $i, 1) =~ /[A-Z\*]/ );
            $frameIntron    = 1 if ( substr($Query, $i, 1) =~ /[A-Z\*]/ && $frameIntron==-1 );
            $frameIntron++      if ( substr($Query, $i, 1) =~ /[a-z]/   && $frameIntron>=1 );
            $lieu++             if ( substr($Query, $i, 1) =~ /[A-Z\*]/ );
            my $line = $matched_symbol.substr($Genom, $i, 1)."\t".substr($Query, $i, 1)."\t".$peptide_pos;
    #Et les mi-mismatches ?
            if ( $matched_symbol eq '|' && $peptide_pos =~ /^\d+$/ ){
                my $triplet = substr($Genom, $i, 1);
                my $j = 0;
                if ( substr($Query, $i, 1) ne '*' ){
                    while( length($triplet) <3 ){
                        $j++;
                        $triplet = $triplet.substr($Genom, ($i+$j), 1) if ( substr($Genom, ($i+$j), 1) =~ /[A-Z]/ );
                    }
                }
                elsif ( substr($Query, $i, 1) eq '*' ){
                    while( length($triplet) <3 ){
                        $j++;
                        $triplet = $triplet.substr($Genom, ($i+$j), 1) if ( substr($Genom, ($i+$j), 1) =~ /[A-Z]/ );
                    }
                    $i = $i+2;
                }
                %pos_aln = (%pos_aln, $peptide_pos => $triplet);
            }
            my $shift = '';
            $shift    = "\t$frameIntron" if ( $frameIntron>0 );
            print {*STDERR} $line, "$shift\n" if ($debug);
            $locus++;
        }
        elsif ( $matched_symbol =~ /\{/ ){
            print {*STDERR} $matched_symbol, substr($Genom, $i, 1), "\t", substr($Query, $i, 1), "\n" if ($debug);
            $frameIntron = -1;
            $accolade++;
        }
        else{
            print {*STDERR} $matched_symbol, substr($Genom, $i, 1), "\t", substr($Query, $i, 1), "\n" if ($debug);
            $lieu++ if ( substr($Query, $i, 1) =~ /U/ );
            if ( substr($Query, $i, 1) =~ /\}/ ){
                if ( $frameIntron==1 ){
                    $boj               = 'o';
                    $pos_boj{$pep_pos} = $boj;
                }
                elsif ( $frameIntron==2 ){
                    $boj               = 'j';
                    $pos_boj{$pep_pos} = $boj;
                }
                $frameIntron = -2 if ( $accolade==1 );
                if ( $accolade==2 ){
                    $frameIntron = 0;
                    $accolade    = 0;
                }
                $boj = '';
            }
            elsif ( $matched_symbol =~ /b/ && $frameIntron==0 ){
                $boj               = 'b';
                $pos_boj{$pep_pos+1} = $boj;
            }
            if ( $matched_symbol =~ /p/ ){
                $frameIntron = 0;
                $boj         = '';
            }
        }
    }


    return(\%pos_aln, \%pos_boj);
}

1;

=head2 sub parser

=over

=item Parse Exonerate output to get only exact matching positions

=item my ($exonerate_output_file) = @_;

=back

=cut

