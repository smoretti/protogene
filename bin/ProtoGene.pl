#!/usr/bin/env perl

########################################################
#                                                      #
# ProtoGene : Bona-Fide Back Translation of Protein    #
#             Multiple Sequence Alignments             #
#                                                      #
########################################################

use strict;
use warnings;
use diagnostics;

use File::Which qw(which);                 # Locate external executable programs in the PATH
use Time::localtime;                       # Use localtime+PID for a pseudo-uniq temp file name
use Getopt::Long;                          # Options specifications
use File::Copy qw(move);                   # Avoid external 'mv' command usage
use LWP::Simple;                           # To test gigablaster availability

use Mail::Send;                            # Send warnings and errors files by e-mail ==> only if the $userEMail variable is defined

use FindBin qw( $RealBin );
use lib "$RealBin/../lib";                 # Local path for ProtoGene's own perl modules
use Exonerate;                             # Exonerate runner, parser, ...
use Views;                                 # Non-text outputs, e.g. HTML/CSS
#use CheckOutput;                         # Check output for cds consistancy with query



################## CONFIGURATION ##################
my $cachePath        = '/scratch/cabernet/monthly/ProtoGene_Cache';   # Cache directory
my $cacheStorageTime = 15;                                            # Do not update sequences younger than X days

my $eutilsEmail      = quotemeta('tcoffee@vital-it.ch');              # Put your e-mail for NCBI eutils requests
my $eutilsTool       = 'ProtoGene';
my $eutilsAPIKey     = '2fc650885ef3f15ca562083e6332e7103c08';
my $userEMail        = 'moretti.sebastien@gmail.com';                 # To receive e-mails with encountered problems; leave blank to inactive

### BLAST parameters ###
my $blast_param = { 'evalue'    => 0.05,
                    'matrix'    => 'BLOSUM62',
                    'filter'    => 'Off',
                    'species'   => 'All_organisms',

                    'db1'       => 'refseq_protein', # NCBI RefSeq protein
                    'identity1' => 100,
                    'cover1'    => 100,
                    'db2'       => 'nr', # NCBI nr protein
                    'identity2' =>  95,
                    'cover2'    =>  95,

                    'nbesthits' => 10,
                    'core'      => 1, # TODO really used ?
                  };
###################################################


my $VERSION  = '4.2.3';

my $webblast_exe  = "$RealBin/../utils/webblast.pl";
my $blast_exe     = 'blastall';        # Or wu-blastall for Wu-BLAST; for local blast usage
my $exonerate_exe = 'exonerate';       # Exonerate 1.0 because current parser only works with this version


################## Option management
my ($msa, $revtrans, $pep, $hideBOJ, $run_name, $template, $lim) = ('', 0, 0, 0, '', '', 0);
my ($cache, $cleancache)                                         = ('update', 'update');#TODO Finish to implement
my ($debug, $tmp)                                                = (0, 0);
my ($db, $species, $local, $giga)                                = ($blast_param->{'db1'}, $blast_param->{'species'}, 0, 0);
my %opts = ('msa|in=s'        => \$msa,        # Input sequences
            'revtrans:s'      => \$revtrans,   # Use to reverse-translate sequences with no match
            'pep'             => \$pep,        # Add the original peptide query beneath the related CDS seq
            'hideBOJ'         => \$hideBOJ,    # Hide BOJ output
            'run_name=s'      => \$run_name,   # Use another name, instead of input seq name, for result files
            'template=s'      => \$template,   # Use a template file
            'lim=i'           => \$lim,        # Limit number of input query sequences
            'cachedir=s'      => \$cache,      # Cache directory
            'cacheclean=s'    => \$cleancache, # Cache behavior

            'orgm|species=s'  => \$species,    # Organism(s) to blast against
            'db|database=s'   => \$db,         # Database to blast against
            'local'           => \$local,      # Use to specify a local db query, defined in $local_db
            'giga'            => \$giga,       # Use GigaBlaster server

            'version|V'       => sub { print "\n\tPROTOGENE : Bona-Fide Back Translation of Protein Multiple Sequence Alignments\n\tversion   : $VERSION\n\n";
                                       exit 0;
                                 },            # Print version information
            'help|H|?'        => sub { system('perldoc', "$0")==0 || print {*STDERR} "\n\tCannot open the documentation with 'perldoc'\n\n";
                                       exit 0;
                                     },        # Print full help message
            'debug'           => \$debug,      # Verbose output
            'tmp'             => \$tmp,        # To keep traces of fake intermediate files like fake xml from NCBI, fake aln, ...
           );
my $test_option_values = Getopt::Long::GetOptions(%opts);
#$revtrans = 1  if ( $revtrans eq '' ); # Allow revtrans to be a boolean or a string option (for tcoffee web server)


################## Short help message
if ( !$test_option_values || ($msa eq '' && $cleancache ne 'empty' && $cleancache ne 'old') ){
    print {*STDERR} "\n\tCannot open the MSA file in FASTA format
\tTry:  $0 --msa=path_of_the_fasta_msa_file [Options]

\tOptions: --orgm      =All_organisms, Bacteria, Viruses, Vertebrata,
\t                      Eukaryota, Mammalia, Primates, Homo_sapiens,
\t                      Gallus_gallus, Bos_taurus, Escherichia_coli,
\t                      Arabidopsis_thaliana, Mus_musculus,
\t                      Drosophila_Melanogaster, ...
\t                      default is '$blast_param->{'species'}'
\t         --db        =nr, pdb, swissprot, refseq_protein
\t                      default is '$blast_param->{'db1'}'
\t         --local      to execute a local BLAST query with
\t                      --db=path_for_a_local_db_blast_formated\n
\t         --template   to provide your own nucleotidic sequences
\t                      following the cds file format
\t         --revtrans   reverse-translates sequences with no
\t                      blast hit, in IUB (IUPAC) depiction code
\t                      They are removed from the alignement by default
\t         --pep        adds the original peptide query beneath the
\t                      back-translated sequence
\t         --cachedir  ='own_PATH_directory'
\t                      (default is '$cachePath')
\t         --cacheclean=none, update, use, old, empty
\t                      to select the cache behavior (default is 'update')\n
\t         --debug      prints extra information when running
\t         --version    prints version information
\t         --help       prints a full help message\n\n";
    exit(1);
}


################## Check cache directory accessibility
if ( -d $cache ){
    $cachePath = $cache;
}
checkCacheAccessibility($cachePath);

# Cache management
$cache = cacheManagement($cache, $cachePath, 0) || '.';

# Remove old files from the cache directory everytime ProtoGene is running
cacheManagement('old', $cachePath, 1);



################## Check external software in the PATH
checkExternalSoftware($webblast_exe, $exonerate_exe);
if ( $local ){
    checkExternalSoftware($blast_exe);
}



################## Check input files
# Open and check template file
my ($template_NT, $template_AA, $template_SEQ);
#TODO: template is fasta only now
($template_NT, $template_AA, $template_SEQ) = checkTemplate($template) if ( $template );


# Open and Check the input fasta file
my $originalMSA    = $msa;
my $converted      = 0; # Accept only fasta format
#TODO: Do a better file converter and identifier is msa is not fasta
#($msa, $converted) = isFastaFile($msa, $converted);

my $fasta_checker = -1;
my (@original_names, @original_seq); #Use 2 lists in parallel : original_names, seq
open( my $MSA, '<', "$msa" ) or die "\n\tCannot open your MSA file\n\n";
READ_MSA:
while(<$MSA>){
    next READ_MSA if ( $_ =~ /^#/ );

    if ( $_ =~ /^>/ ){
        $fasta_checker++;
        my $name = $_;
        $name    =~ s{\r\n}{}g; #Remove return lines from windows OS '^M'
        $name    =~ s{\r}{\n}g;
        $name    =~ s{^>[ \t]+}{>};
        chomp($name);

        push @original_names, $name;
    }
    elsif ( $_ =~ /[A-Z\-\.]/i ){
        my $sequence = $_;
        $sequence    =~ s{\r\n}{}g;
        $sequence    =~ s{\r}{\n}g;
        $sequence    =~ s{\.}{-}g; #for msa with '.' as gap
        #TODO because exonerate cannot work with selenocystein
        $sequence    =~ s{[BJOUZ]}{X}ig; #U here for selenocystein
        $sequence    =~ s{[^A-Za-z\-\*\n]}{}g; #Remove all the non-gap or non-alphabetic characters from the seq
        chomp($sequence);

        #fasta sequence on 1 line
        if ( !exists($original_seq[$fasta_checker]) ){
            push @original_seq, $sequence;
        }
        else {
            $original_seq[$fasta_checker] .= $sequence;
        }
    }
}
close $MSA;

#Are there FASTA sequences ?
if ( $fasta_checker == -1 ){
    failure();
    print {*STDERR} "\tThe MSA file does NOT seem to be a protein FASTA format
\tSee an example of FASTA format <a href='http://en.wikipedia.org/wiki/FASTA_format' target='_blank'>here</a>\n\n";
    exit(1);
}
elsif ( $lim >0 && $fasta_checker > $lim ){
    failure();
    print {*STDERR} "\tThe FASTA file is too large, try with less than $lim sequences\n\tor split your file\n\n";
    exit(1);
}
elsif ( exists( $original_seq[0] ) && $original_seq[0] =~ /[acgtu]/i ){
    my $first_seq = $original_seq[0];
    #Check if sequences are amino acids and not nucleotides ONLY on the 1st sequence
    my $a         = ($first_seq =~ s{[aA]}{}g)               || 0;
    my $c         = ($first_seq =~ s{[cC]}{}g)               || 0;
    my $g         = ($first_seq =~ s{[gG]}{}g)               || 0;
    my $t         = ($first_seq =~ s{[tTuU]}{}g)             || 0;
    my $non       = ($first_seq =~ s{[^aAcCgGtTuUXxNn-]}{}g) || 0;
    if ( ($a+$c+$g+$t) >= (($a+$c+$g+$t+$non)*80/100) ){
        failure();
        print {*STDERR} "\tYour sequences look already to be nucleotides
\tThe goal of this program is to turn AMINO ACID alignments into CDS nucleotide alignments\n\n";
        exit(1);
    }
}
undef $fasta_checker;


# Check GigaBlaster status if used
if ( $giga==1 ){
    $local = 0;
    $local = 2  if ( head('http://www.igs.cnrs-mrs.fr/public_g/remoteblast.cgi') );
}


# Temporary file extension
my $date = sprintf("%d-%02d-%02d_%02dh%02d", localtime->year() + 1900, localtime->mon() + 1, localtime->mday(), localtime->hour, localtime->min);



#Start main program with version # of programs and list original queries
print "\n\t   Protogene\t$VERSION\t$date\n\n";

open(my $EXONERATEISHERE, "$exonerate_exe --version |");
my $IsExonerateHere = <$EXONERATEISHERE>;
close $EXONERATEISHERE;
print "\t   $IsExonerateHere\n";
undef $IsExonerateHere;
undef $VERSION;

for(my $m=0; $m<=$#original_names; $m++){
    printf("\t>%-3s  is  %s\n", $m, $original_names[$m]);
}
$date .= "_$$"; #Add PID to be more uniq



# run_name implementation for web server
$originalMSA = $run_name  if ( $run_name ne '' );

unlink("$originalMSA.cds", "$originalMSA.cdsP", "$originalMSA.cdsP.html",
       "$originalMSA.out", "$originalMSA.boj",  "$originalMSA.log");


# Build the sequences, from the alignment, to perform blast search
EACH_SEQ:
for(my $r=0; $r<=$#original_names; $r++){
    my $fasta_header = $original_names[$r];
    my $short_name   = $original_names[$r];
    $fasta_header    =~ s{^>}{};               # Full FASTA header
    $short_name      =~ s{^([^\s\t]+).*$}{$1}; # Short name without description


    my @equivalent_blast_hits; #Multiple BLAST hits with the same highest e-value/score

    if ( exists($template_NT->{$short_name}) && $template_NT->{$short_name} ne '' ){
        # First try nucleotide template
        push @equivalent_blast_hits, 'My_own_seq';
        print "\n\n$r done\n\nNucleotide template for $r:\t$template_NT->{$short_name}\n\n*******************************************************************\n\n";
    }
    elsif ( exists($template_AA->{$short_name}) && $template_AA->{$short_name} ne '' ){
        # Second try amino acid/BLASTp template
        push @equivalent_blast_hits, $template_AA->{$short_name};
        print "\n\n$r done\n\nBlastP template for $r:\t$template_AA->{$short_name}\n\n*******************************************************************\n\n";
    }
    else{
    # else send them to webblast
        # Prepare sequence $r for BLAST
        open(my $READY2BLAST, '>', "$cache/$date.seq2blast.fas.$r");
        my $noGap = $original_seq[$r];
        $noGap    =~ s{\-}{}g; #remove gaps
        print {$READY2BLAST} ">$r\n$noGap\n";
        close $READY2BLAST;

        if ($r==0){
            # Show BLAST parameters only for the first BLAST run
            display_blast_param();
        }
        run_BLAST("$cache/$date.seq2blast.fas.$r", $local, $blast_param->{'db1'}, $blast_exe, "$cache/$date.blastp.$r");


        # Get the BLASTp hit(s)  acc number
        open(my $BLASTP, '<', "$cache/$date.blastp.$r");
        while(<$BLASTP>){
            push @equivalent_blast_hits, $1  if ( $_ =~ /^>.+?\@.+?__(\S+).*$/ ); #Warning: double '_'
        }
        close $BLASTP;
        unlink("$cache/$date.blastp.$r", "$cache/$date.seq2blast.fas.$r")  if ( $tmp == 0 || exists($equivalent_blast_hits[0]) );
        if ( !exists($equivalent_blast_hits[0]) ){
            print "\n$r ...\tNo blast result found above thresholds for $fasta_header\n\n";
            buildFailureOutputFiles($r, 'No_BLASTp_Result', 'Not_searched');
            undef $original_seq[$r];
            undef $original_names[$r];
            next EACH_SEQ;
        }
    }



    #When multiple equivalent blast hits  ==> use, and add, every hits
    my ($resultPOS, $resultBOJ) = ('', '');
    my @failureStatus = (''); # To always have $failureStatus[0] defined
    my @intronStatus  = ('');
    HIT_LINK:
    for(my $qq=0; $qq<=$#equivalent_blast_hits; $qq++){
        print "\n$fasta_header  -->  [$equivalent_blast_hits[$qq]]\n";


        my @nt_GIs;
        my $intronStep = 0;
        # Get Nt sequence from template
        if ( exists($template_NT->{$short_name}) && $template_NT->{$short_name} ne '' ){
            if ( $template_NT->{$short_name} !~ /^My_Seq$/i ){
                download_seq($cache, '', '', $template_NT->{$short_name});
                @nt_GIs = $template_NT->{$short_name};
            }
            else {
                open(my $MYSEQ, '>', "$cache/My_Seq-$$.fas");
                print {$MYSEQ} ">My_Seq-$$ for $short_name\n".$template_SEQ->{$short_name}."\n";
                close $MYSEQ;
                @nt_GIs = "My_Seq-$$";
            }
        }
        # Get Nt sequence through prot acc -> prot gi -> nt linked gi -> nt acc
        else{
            # Prot ACC -> PUID
            my $protGI = blastPAcc2PGI($equivalent_blast_hits[$qq]);
            if ( $protGI eq '' ){
                print "\tNo protein (prot GI) link found for $equivalent_blast_hits[$qq] in $fasta_header\n\n";
                @failureStatus = ('PUI_unavailable', $equivalent_blast_hits[$qq]);
                next HIT_LINK;
            }

            # PUID -> nt GIs & GeneID
            my ($ntGIs, $geneID) = protGI2NTGIs($protGI, $equivalent_blast_hits[$qq]);
            if ( $ntGIs eq '' && $geneID eq '' ){
                print "\tNo nucleotide (nt GIs) link found for $equivalent_blast_hits[$qq] in $fasta_header\n\n";
                @failureStatus = ('No_nt_link', $equivalent_blast_hits[$qq]);
                next HIT_LINK;
            }
            print " linked with [$ntGIs $geneID]\n";


            # Get transcript and contig seq
            if ( $ntGIs ne '' ){
                @nt_GIs = split(/,/, $ntGIs);
                downloadSeqFromGIs($cache, '', '', @nt_GIs);
            }

            # GeneID -> Chr
            if ( $geneID ne '' ){
                my @geneIDs = split(/,/, $geneID);
                while(<@geneIDs>){
                    my $geneID = $_;
                    my ($chr, $amont, $aval) = geneID2Chr($geneID, $equivalent_blast_hits[$qq]);
                    print "\n\tNo gene locus found for $equivalent_blast_hits[$qq] with $geneID in $fasta_header\n\n" if ($chr eq '' and $geneID ne '');

                    # Get Chr seq
                    if ( $chr ne '' ){
                        download_seq($cache, $amont, $aval, $chr);
                        $intronStep = 1;

                        unshift(@nt_GIs, "$chr--$amont-$aval");
                    }
                }
            }
        }


        # Align Nt seq with our protein query seq
        my ($POS, $BOJ) = runExonerate($cache, $date, $r, $original_seq[$r], $fasta_header, @nt_GIs);
        next HIT_LINK if ( !defined $POS );

        my ($POSresult, $bestNucleotide) = prepareResults4CDS($POS, $equivalent_blast_hits[$qq], $original_seq[$r], $fasta_header);
        $resultPOS .= $POSresult if ( $bestNucleotide ne '' && $POSresult ne '' && $resultPOS !~ /_G_$bestNucleotide/ );

        @intronStatus = ($POS->{0}, $equivalent_blast_hits[$qq], '')              if ( $intronStep==1 && exists($POS->{0}) && $POS->{0} =~ /\:/ );
        @intronStatus = ($POS->{0}, $equivalent_blast_hits[$qq], $bestNucleotide) if ( $intronStep==0 && $bestNucleotide =~ /^NA[CTWZGS]_/ );

        my ($intronLess, $nameLess) = ('', '');
        $intronLess = $POS->{0}       if ( %$BOJ eq 0 && exists($POS->{0}) && $POS->{0} ne '' );
        $nameLess   = $bestNucleotide if ( %$BOJ eq 0 && $intronStep==0 && $bestNucleotide =~ /^NA[CTWZGS]_/ );
        my ($BOJresult, $bestGenomic) = prepareResults4BOJ($BOJ, $equivalent_blast_hits[$qq], $original_seq[$r], $fasta_header, $intronLess, $nameLess);
        $resultBOJ .= $BOJresult      if ( $bestGenomic ne '' && $BOJresult ne '' && $resultBOJ !~ /_G_$bestGenomic[ \-]/ );
    }


    if ( $resultPOS eq '' ){
        if ( $failureStatus[0] ne '' ){
            buildFailureOutputFiles($r, $failureStatus[1],         $failureStatus[0]);
        }
        else {
            buildFailureOutputFiles($r, $equivalent_blast_hits[0], 'Alignment_failure');
        }
        next EACH_SEQ;
    }
    elsif ( $resultBOJ eq '' ){
        if ( $failureStatus[0] eq 'PUI_unavailable' || $failureStatus[0] eq 'No_nt_link' ){
            buildFailureBOJOutputFile($failureStatus[1], $failureStatus[0], $fasta_header, $original_seq[$r]);
        }
        elsif ( $failureStatus[0] eq '' && $intronStatus[0] ne '' ){
            buildIntronlessBOJOutputFile($intronStatus[1], $intronStatus[0], $intronStatus[2], $fasta_header, $original_seq[$r]);
        }
        else{
            buildFailureBOJOutputFile($equivalent_blast_hits[0], 'Unavailable', $fasta_header, $original_seq[$r]);
        }
        createCDSOutputFile($resultPOS, $original_seq[$r], $fasta_header);
    }
    else {
        createCDSOutputFile($resultPOS, $original_seq[$r], $fasta_header);
        createBOJOutputFile($resultBOJ);
    }


    undef $original_seq[$r];
    undef $original_names[$r];
}


checkAndCleanStderrFiles("$cache/$date.ExonerateError")  if ( -e "$cache/$date.ExonerateError" );
checkOutputFiles();
unlink("$msa")  if ( $converted==1 );
unlink 'blast_result.txt';
my @tmpFiles = glob($cache."/$date*");
if ( exists($tmpFiles[0]) ){
    mkdir("$cache/$date.TMP");
    move("$_", "$cache/$date.TMP")  while(<@tmpFiles>);
}


#Ouput Checker


#STDOUT Log output for our server
if ( -s "$originalMSA.cds" || -s "$originalMSA.out" ){
    print "\n\nTERMINATION STATUS: SUCCESS\n";
    print 'OUTPUT RESULTS', "\n";
    print "    #### File Type=        MSA Format= fasta_CDS Name= $originalMSA.cds\n"
        if ( -s "$originalMSA.cds" );
    print "    #### File Type=        MSA Format= fasta_CDS+Query Name= $originalMSA.cdsP\n"
        if ( -s "$originalMSA.cdsP" );
    print "    #### File Type=        MSA Format= colored_fasta_CDS Name= $originalMSA.cdsP.html\n"
        if ( Views::Html("$originalMSA.cdsP") && -s "$originalMSA.cdsP.html" );
    print "    #### File Type=        MSA Format= Rejected_seq Name= $originalMSA.out\n"
        if ( -s "$originalMSA.out" );
    print "    #### File Type=        MSA Format= fasta_Exon-Boundaries Name= $originalMSA.boj\n"
        if ( -s "$originalMSA.boj" && -s "$originalMSA.cds" && $hideBOJ==0 );

    unlink "$cache/web_tempo.result";
}
else{
    failure();
    exit(1);
}

exit(0);


=head1 NAME

ProtoGene.pl - Converts a peptidic alignment to a nucleotidic alignment through database search

=head1 SYNOPSIS

B<ProtoGene.pl> --msa=path_of_the_fasta_msa_file [options]

with these options available:

=over 4

=item I<--orgm>=targeted species in the database search

=item All_organisms [default], Bacteria, Viruses, Vertebrata, Eukaryota, Mammalia, Primates, Homo_sapiens, Gallus_gallus, Bos_taurus, Escherichia_coli, Arabidopsis_thaliana, Mus_musculus, Drosophila_Melanogaster, ...

=item I<--db>=targeted database in the database search

=item nr, pdb, swissprot, refseq_protein [default]

=item I<--local> to execute a local BLAST query with  --db=path_for_a_local_db_blast_formated

=item I<--template> to provide your own nucleotidic sequences following the cds file format

=item I<--revtrans> to reverse-translate sequences with no blast hit, in IUB (IUPAC) depiction code

=item They are removed from the alignement by default

=item I<--pep> to add the original peptide query beneath the back-translated sequence

=item I<--cachedir>=your_own_path_directory

=item cachedir sets cache directory

=item I<--cacheclean>=none, update, use, old, empty

=item cacheclean manages cache behavior

=item none: no cache usage, none temporary files are stored

=item update: use cache but update old files [default]

=item use: force use cache, whatever the age of files

=item old: remove the old files in the cache directory

=item empty: empty completely the cache directory

=item I<--version> to print version information

=item I<--help> to print this help message

=back

=head1 DESCRIPTION

PROTOGENE : Bona-Fide Back Translation of Protein Multiple Sequence Alignments
It converts a peptidic alignment to a nucleotidic alignment through database search.
Blast queries allow to get nucleotidic sequences from where peptidic sequences come from.
Exonerate aligns nucleotidic and peptidic sequences together.
PROTOGENE re-builds the original alignment with nucleotidic information it has gotten.

=head1 REQUIREMENT

=over 4

=item B<Perl 5.6 or better> is required !

=item Standard Perl modules B<lib>, B<strict>,  B<warnings>,  B<diagnostics> are required

=item as well as some other current ones : B<Getopt::Long>,  B<File::Which>,  B<File::Copy>, B<Mail::Send>, B<Time::localtime>, B<LWP::Simple>

=item -

=item I<exonerate> version 2 from http://www.ebi.ac.uk/about/vertebrate-genomics/software/exonerate

=item I<blast> from ftp://ftp.ncbi.nih.gov/blast/executables/

=back

=head1 VERSION

=over 8

=item version 4.2.2

=item on Sep 09th, 2016

=back

=head1 AUTHORS

=over 8

=item Sebastien MORETTI

=item moretti.sebastien [AT] gmail.com

=item Vital-IT computing center

=item SIB Swiss Institute of Bioinformatics

=item Lausanne, Switzerland

=item https://www.vital-it.ch/

=back

=cut


######################  Management  ######################

# Failure declaration
sub failure {
    print {*STDERR} "\n\nFATAL\n\nTERMINATION STATUS: FAILURE\n\n";
    return;
}

# Check external programs presence in the PATH
sub checkExternalSoftware {
    for my $exe ( @_ ){
        if ( ! which($exe) && !-x $exe ){
            failure();
            print {*STDERR} "\t'$exe' program is not reachable\n\tIt could not be in your PATH or not installed\n\n";
            exit(1);
        }
    }
    return;
}

# Check cache directory accessibility: Test and change rights, if required, of the cache directory
sub checkCacheAccessibility {
    my ($cacheDir) = @_;

    if ( -d "$cacheDir/" && -w "$cacheDir/" && -x "$cacheDir/" && -r "$cacheDir/" ){
        # Good
    }
    elsif ( -d "$cacheDir/" ){
        if ( -o "$cacheDir/" ){
            chmod 0754, "$cacheDir/";
        }
        else{
            failure();
            print {*STDERR} "\tPermissions do NOT seem to be valid for the '$cacheDir' cache directory\n\n";
            exit(2);
        }
    }
    else{
        mkdir "$cacheDir/"; # FIXME: mkdir is not recursive !
        if ( -o "$cacheDir/" ){
            chmod 0754, "$cacheDir/"; #0754 is in octal, so 754 or '0754' are not good !
        }
        else{
            failure();
            print {*STDERR} "\tPermissions do NOT seem to be valid for the '$cacheDir' cache directory\n\n";
            exit(2);
        }
    }

    return;
}

# Cache Behavior Management
sub cacheManagement{
    my ($cache, $cacheDir, $inScript) = @_;

    if ( $cache eq 'empty' ){
        unlink <$cacheDir/*.fas>;
        unlink <$cacheDir/*.ExonerateError>;
        unlink("$cacheDir/webblast.log", "$cacheDir/web_tempo.result", "$cacheDir/debug.tempo");
        print "\n\tcache directory [$cacheDir] is empty\n\n";
        exit 0;
    }
    elsif ( $cache eq 'old' ){
        unlink    grep { -M $_ > $cacheStorageTime }    glob($cacheDir.'/*.fas'); # Grep list only with $cacheStorageTime days old files

        if ( $inScript==0 ){
            print "\n\tcache directory [$cacheDir] is empty of its old files\n\n";
            exit 0;
        }
    }
    elsif ( $cache eq 'update' || $cache eq 'use' ){
        # DEFAULT: Limit cached files to less than $cacheStorageTime days old ones for 'update'
        $cache = $cacheDir;
    }
    elsif ( $cache eq 'none' ){
        # Do not use cache
    }
    elsif ( -d $cache ){
        die "\n\tProtoGene cannot access to your directory\n\n"  if ( ! -x $cache );
        die "\n\tProtoGene cannot write into your directory\n\n" if ( ! -w $cache );
        die "\n\tProtoGene cannot read into your directory\n\n"  if ( ! -r $cache );
    }
    else {
        failure();
        print {*STDERR} "\n\tWrong cache argument, or your cache folder '$cacheDir' doesn't seem to exist\n\n";
        exit(3);
    }

    return($cache);
}

##########################################################


######################    Fasta    ######################

# Check input aln format
sub isFastaFile{
    my ($msa, $converted) = @_;

    if ( -s "$msa" ){ # Check is the file exists and is not empty
        # Is FASTA format ?
        my $firstLine = `head $msa`;
        return($msa, 0) if ( $firstLine =~ /^>/ );

        # Convert no fasta format to fasta with readseq
        my $reformat = which('readseq') || '';
        if ( $reformat ne '' ){
            system("$reformat -a -f8 $msa > $msa.007")==0 or do {failure(); print {*STDERR} "\n\tEnable to convert your file to fasta format\n\n";};
            $msa      .= '.007';
            $converted = 1;
        }
    }
    else {
        failure();
        print {*STDERR} "\tThe file \'$msa\' doesn't not seem to be reachable\n\n";
        exit(1);
    }

    return($msa, $converted);
}

#Prepare Fasta Header of the query name to incorporate our annotations
sub fastaHeaders4ProtoGene{
    my ($QueryName) = @_;

    $QueryName =~ s{  +}{ }g;
    my ($acc, $desc) = ($QueryName, $QueryName);
    $acc  =~ s{^ *([^ ]*) .*$}{$1};
    $acc  =~ s{\s+$}{}g;
    $acc  =  $acc.'_G_@@';

    $desc =~ s{^ *[^ ]* *(.*)$}{$1};
    $desc =~ s{^[\| \.]+}{};
    $desc =~ s{[\|\. ]+$}{};

    return($acc.$desc);
}

#Prepare Fasta Header to put it on the fasta header of the result outputs
sub getFastaHeaderAnnot{
    my ($bestTarget) = @_;

    #Get annotation from nucleotide file header
    open(my $BEST, '<', "$cache/$bestTarget.fas");
    my $annot = <$BEST> || '';
    close $BEST;

    chomp($annot);
    $annot =~ s{^ *[^ ]* }{};
    $annot =~ s{[\. ]*$}{};
    $annot =~ s{^ *}{};
    $annot =~ s{  +}{ }g;

    return($annot);
}

#########################################################


###################### Translation ######################

#Direct Reverse translation
sub reverse_trad{
    my ($aa) = @_;

    my %reverse_code = ('A' => 'GCN', 'F' => 'TTy', 'K' => 'AAr', 'P' => 'CCN', 'T' => 'ACN',
                        'C' => 'TGy', 'G' => 'GGN', 'L' => 'yTN', 'Q' => 'CAr', 'V' => 'GTN',
                        'D' => 'GAy', 'H' => 'CAy', 'M' => 'ATG', 'R' => 'mGN', 'W' => 'TGG',
                        'E' => 'GAr', 'I' => 'ATh', 'N' => 'AAy', 'S' => 'wsN', 'Y' => 'TAy',
                        'X' => 'NNN', '-' => '---', '*' => 'Trr',
                       ); # '*' is for stop codon

    my $triplet = 'nnn';
    while( my ($x, $y) = each(%reverse_code) ){
        if ( uc($aa) eq $x ){
            $triplet = $y;
            last;
        }
    }

    return($triplet);
}

#########################################################


####################### webblast #######################
sub display_blast_param {
    print "
             Program :                   BLASTp
             Evalue threshold :          $blast_param->{'evalue'}
             Matrix :                    $blast_param->{'matrix'}
             Filter :                    $blast_param->{'filter'}
             Equivalent best hits used : $blast_param->{'nbesthits'}

             Database:                   NCBI $blast_param->{'db1'}
             Blast_identity_threshold :  $blast_param->{'identity1'}
             Cover threshold :           $blast_param->{'cover1'}
          if no match
             Database:                   NCBI $blast_param->{'db2'}
             Blast_identity_threshold :   $blast_param->{'identity2'}
             Cover threshold :            $blast_param->{'cover2'}
\n";
    return;
}

sub run_BLAST{
    my ($infile, $whatBlast, $db, $blast_exe, $outfile) = @_;

    my $blast_at = $whatBlast==2  ? '-gigablast=yes'
                 : $whatBlast==1  ? "-blast_dir=$blast_exe"
                 :                  '';

    # BLAST run 1
    system("$webblast_exe -program=blastp -database=$db $blast_at -infile=$infile -matrix=$blast_param->{'matrix'} -evalue=$blast_param->{'evalue'} -method=geneid -filter=$blast_param->{'filter'} -organism=$species -identity=$blast_param->{'identity1'} -cover=$blast_param->{'cover1'} -hits=$blast_param->{'nbesthits'} -quiet=on -outfile=$outfile");


    if ( -z "$outfile" ){
    # BLAST run 2 if no hit with 100% identity and coverage: refseq100 -> nr95  OR  nr100 -> nr95
        print "run BLAST.. against $blast_param->{'db2'} because there was no acceptable hit against $blast_param->{'db1'}\n"
            if ( $whatBlast != 1 && $db ne 'nr' );
        print "run BLAST.. again, with decreased thresholds, because there was no acceptable hit against $db\n"
            if ( $whatBlast == 1 || $db eq 'nr' );

        system("$webblast_exe -program=blastp -database=$blast_param->{'db2'} $blast_at -infile=$infile -matrix=$blast_param->{'matrix'} -evalue=$blast_param->{'evalue'} -method=geneid -filter=$blast_param->{'filter'} -organism=$species -identity=$blast_param->{'identity2'} -cover=$blast_param->{'cover2'} -hits=$blast_param->{'nbesthits'} -quiet=on -outfile=$outfile");
    }

    move('webblast.log',     "$cache"); #Move temporary webblast.pl files
    move('web_tempo.result', "$cache");
    move('debug.tempo',      "$cache");

    unlink("$cache/webblast.log")     if ( -z "$cache/webblast.log" );
    unlink("$cache/web_tempo.result") if ( -z "$cache/web_tempo.result" );
    unlink("$cache/debug.tempo")      if ( -z "$cache/debug.tempo" );

    return;
}


########################################################


##################### NCBI requests #####################
sub fetch {
    my ($url) = @_;

    XML:
    for (my $tries=0; $tries <20; $tries++ ){
        my $content = get($url);
        print {*STDERR} "[$content]\n\n" if ($debug);
        if ( defined $content ){
            next XML  if ( $content =~ /<ERROR>/i && $content !~ /<ERROR>Can not find description/i );
            return $content;
        }
    }
    print {*STDERR} "\n\tERROR: Problem with NCBI eutils, please try again later\n";
    print {*STDOUT} "\n\tERROR: Problem with NCBI eutils, please try again later\n";
    exit(4);
}

sub fetch_fasta {
    my ($url, $outfile) = @_;

    return 1 if ( -s "$outfile" && $cache eq 'update' && -M "$outfile" <= $cacheStorageTime );

    GET_FASTA:
    for (my $tries=0; $tries <20; $tries++ ){
        my $HTTP_status = getstore($url, $outfile);
        if ( is_error($HTTP_status) ){
            next GET_FASTA;
        }
        else {
            open(my $FASTA, '<', "$outfile");
            my $counter = 0;
            my $lines   = 0;
            while(<$FASTA>){
#                $lines++              if ( $_ !~ /^>/ ); #Allow download of entries with only fasta header and no sequence associated
                $counter = $counter+2 if ( $counter==1 && $_ !~ /^\w/ && $lines==1 && $_ !~ /^>/ );
                $counter++            if ( $_ =~ /^>/ );
                $counter = $counter+2 if ( $_ !~ /^>/ && ($_ =~ /Error:/ || $_ =~ /[<>]/) );
            }
            close $FASTA;

            return 1 if ( $counter == 1 );
        }
    }
    print {*STDERR} "[[$url]]\n" if ( $debug );
    unlink("$outfile");
    print {*STDERR} "\n\tERROR: Problem with NCBI eutils, please try again later.\n";
    print {*STDOUT} "\n\tERROR: Problem with NCBI eutils, please try again later.\n";
    exit(5);
}


#Prot ACC -> PUID
sub blastPAcc2PGI{
    my ($blastHit) = @_;

    my $protGI = '';
    #FIXME: should be ${blastHit}[pacc] but something is broken at NCBI
    my $content = fetch("https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=protein&term=$blastHit&retmode=xml&tool=$eutilsTool&email=$eutilsEmail&api_key=$eutilsAPIKey");
    if ( $content =~ /<Id>(\d+)<\/Id>/ ){
        $protGI = $1;
    }

    return($protGI);
}

#PUID -> nt GIs
sub protGI2NTGIs{
    my ($protGI, $blastHit) = @_;

    my $ntGIs  = '';
    my $geneID = '';
    my $content = fetch("https://eutils.ncbi.nlm.nih.gov/entrez/eutils/elink.fcgi?dbfrom=protein&db=nuccore,gene&id=$protGI&retmode=xml&tool=$eutilsTool&email=$eutilsEmail&api_key=$eutilsAPIKey");

    my @xml  = split("\n", $content);
    my $flag = 0;
    for my $line (@xml){
        if (    $line =~ /<LinkName>protein_nuc[a-z]+<\/LinkName>/ ){ #for nuccleotide and nuccore
            $flag = 1;
        }
        elsif ( $line =~ /<LinkName>protein_gene<\/LinkName>/ ){
            $flag  = 2;
        }
        elsif ( $flag==1 && $line =~ /^.*<Id>(\d+)<\/Id>.*$/ ){
            my $match = $1;
            $ntGIs  .= "$match,$match,";
        }
        elsif ( $flag==2 && $line =~ /^.*<Id>(\d+)<\/Id>.*$/ ){
            my $match = $1;
            $geneID .= "$match,$match,";
        }
    }

    #Remove redundancy if any
    chop $ntGIs; #Remove last ','
    my %hash_NT = split(',', $ntGIs);
    $ntGIs      = join(',',  keys(%hash_NT) );
    chop $geneID;
    my %hash_GE = split(',', $geneID);
    $geneID     = join(',',  keys(%hash_GE) );

    return($ntGIs, $geneID);
}

sub geneID2Chr{
    my ($geneID, $blastHit) = @_;

    my $chr   = '';
    my ($amont, $aval) = ('', '');
#   my $content = fetch("https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=gene&id=$geneID&retmode=xml&tool=$eutilsTool&email=$eutilsEmail&api_key=$eutilsAPIKey");
    my $content = fetch("https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?db=gene&id=$geneID&retmode=xml&tool=$eutilsTool&email=$eutilsEmail&api_key=$eutilsAPIKey");

    my @xml  = split("\n", $content);
    my $flag = 0;
    GENE_INFO:
    for my $line (@xml){
        if ( $flag==0    && $line =~ /<Item Name="GenomicInfoType" Type="Structure">/ ){
            $flag = 1;
        }
        elsif ( $flag==1 && $line =~ /<Item Name="ChrAccVer" Type="String">([^<]+?)\.?\d*<\/Item>/ ){
            $chr   = $1;
            $flag  = 3;
        }
        elsif ( $flag==3 && $line =~ /<Item Name="ChrStart" Type="Integer">(\d+)<\/Item>/ ){
            $amont = $1;
            $flag  = 4;
        }
        elsif ( $flag==4 && $line =~ /<Item Name="ChrStop" Type="Integer">(\d+)<\/Item>/ ){
            $aval  = $1;
            last GENE_INFO;
        }
    }

    if ( $amont eq '' || $aval eq '' ){
        $amont = '';
        $aval  = '';
    }
    elsif ( $amont ne '' && $aval ne '' ){
        if ( $amont > $aval ){
            $amont = $amont + 5000;
            $aval  = $aval  - 5000;
        }
        else{
            $amont = $amont - 5000;
            $aval  = $aval  + 5000;
        }
    }
    $amont = 1 if ( $amont ne '' && $amont <= 0 );
    $aval  = 1 if ( $aval  ne '' && $aval  <= 0 );

    return($chr, $amont, $aval);
}

sub downloadSeqFromGIs{
    my ($cache, $amont, $aval, @acc) = @_;

    GET_SEQ:
    for(my $a=0; $a<=$#acc; $a++){
        if ( $amont =~ /^\d+$/ && $aval =~ /^\d+$/ ){
            fetch_fasta("https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=nucleotide&id=$acc[$a]&rettype=fasta&retmode=text&from=$amont&to=$aval&tool=$eutilsTool&email=$eutilsEmail&api_key=$eutilsAPIKey", "$cache/$acc[$a]-$amont.fas") ;
        }
        else{
            fetch_fasta("https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=nucleotide&id=$acc[$a]&rettype=fasta&retmode=text&tool=$eutilsTool&email=$eutilsEmail&api_key=$eutilsAPIKey", "$cache/$acc[$a].fas");
        }
    }

    return;
}

sub download_seq{
    my ($cache, $amont, $aval, @acc) = @_;

    my $cp   = 0;
    my $from = $amont;
    my $to   = $aval;
    DOWNL_SEQ:
    for(my $a=0; $a<=$#acc; $a++){
        my $pacc2puid = $acc[$a];

        if ( $pacc2puid !~ /^[NAX][CGTSWZMR]_/ ){ #Not RefSeq acc
        #pacc = primary acc NOT prot acc !   #265666 -> S55551
            my $content = fetch("https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=nucleotide&term=${pacc2puid}[pacc]&tool=$eutilsTool&email=$eutilsEmail&api_key=$eutilsAPIKey");
            if ( $content =~ /<Id>(\d+)<\/Id>/ ){
                $pacc2puid = $1;
            }
        }

        if ( $amont =~ /^\d+$/ && $aval =~ /^\d+$/ ){
            fetch_fasta("https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=nucleotide&id=$pacc2puid&rettype=fasta&retmode=text&from=$amont&to=$aval&tool=$eutilsTool&email=$eutilsEmail&api_key=$eutilsAPIKey", "$cache/$acc[$a]--$from-$to.fas");
        }
        else{
            fetch_fasta("https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=nucleotide&id=$pacc2puid&rettype=fasta&retmode=text&tool=$eutilsTool&email=$eutilsEmail&api_key=$eutilsAPIKey", "$cache/$acc[$a].fas");
        }

        #FIXME Don't remember exactly what all this function does
        my $checkSeq = '';
        $checkSeq    = `grep -v '>' "$cache/$acc[$a]--$from-$to.fas"` if ( $amont =~ /^\d+$/ && $aval =~ /^\d+$/ );
        $checkSeq    = `grep -v '>' "$cache/$acc[$a].fas"`            if ( $amont !~ /^\d+$/ || $aval !~ /^\d+$/ );
        if ( $checkSeq !~ /^[A-Za-z]/ && $cp==0 ){
#       if ( -s "$cache/$acc[$a]--$from-$to.fas" <120 && $cp==0){ #Instead of multiple downloads, only seq name and header
            unlink("$cache/$acc[$a]--$from-$to.fas") if ( $amont =~ /^\d+$/ && $aval =~ /^\d+$/ );
            unlink("$cache/$acc[$a].fas")            if ( $amont !~ /^\d+$/ || $aval !~ /^\d+$/ );
#           $amont = '';
            $aval  = $aval - 5000 if ( $aval =~ /^\d+$/ );
            $a     = $a    - 1;
            $cp++;
        }
    }

    return;
}
#########################################################



###################### Exonerate #######################
sub runExonerate{
    my ($cache, $date, $order, $original, $fasta_header, @gis) = @_;

    # Prot to align to nucleotidic sequence(s)
    open(my $FASTA, '>', "$cache/$date.$order.fas");
    my $seq2aln = $original;
    $seq2aln    =~ s{-}{}g;
    $seq2aln   .= '*'  if ( $seq2aln !~ /\*$/ ); # Add artificially stop codon triplet at the end of the protein seq
                                                 # * is aligned only if there is one stop codon similar at the last position of protein
                                                 # If not, * is excluded from the alignment because of external/local border misalignment
    print {$FASTA} ">$order\n$seq2aln\n";
    close $FASTA;


    my $best_pos;
    my $best_boj;
    my @bestAln;
    RUN_EXONERATE:
    for(my $b=0; $b<=$#gis; $b++){
        # Warning: Arbitrarily threshold between genomic and transcript sequence lengths, fixed at 5000 bp here
        # To manage short DNA seq, everything pass through --model protein2genome but with --exhaustive for short ones
        if ( -s "$cache/$gis[$b].fas" > 5000 ){
            # Exonerate prot - genomic or long RNA
            system("$exonerate_exe --showquerygff --showtargetgff --model protein2genome --bestn 1 --percent 75 -q $cache/$date.$order.fas -t $cache/$gis[$b].fas >$cache/$date.$order.exon 2>>$cache/$date.ExonerateError");
        }
        else{
            # Exonerate prot - short genomic or short RNA
#            system("$exonerate_exe --showquerygff --showtargetgff --model protein2genome --bestn 1 --percent 75 --exhaustive -q $cache/$date.$order.fas -t $cache/$gis[$b].fas >$cache/$date.$order.exon 2>>$cache/$date.ExonerateError");
            system("$exonerate_exe --showquerygff --showtargetgff --model protein2genome --bestn 1 --percent 75 -q $cache/$date.$order.fas -t $cache/$gis[$b].fas >$cache/$date.$order.exon 2>>$cache/$date.ExonerateError"); # Since centos7 update --exhaustive option looks to turn into an infinite loop!
        }

        my $targetNT = $gis[$b];
        $targetNT = 'gi|'.$targetNT.'|'  if ( $gis[$b] =~ /^\d+$/ );
        print {*STDERR} "\n@@ -> $b ... $targetNT\n"  if ( $debug );


        # Remove exonerate Error file if it has failed to align protein and nucleotide sequences
        unlink("$cache/Error:")  if ( $tmp == 0 || -z "$cache/Error:" );


        if ( !-e "$cache/$date.$order.exon" || -s "$cache/$date.$order.exon" < 520 ){
            print "\tProtein-Nucleotide alignment has   FAILED   for $targetNT with $fasta_header\n";
            next RUN_EXONERATE;
        }
        print "\tProtein-Nucleotide alignment was successful for $targetNT with $fasta_header\n";


        # Parse Exonerate output
        my ($posiTions, $posBOJ) = Exonerate::parser("$cache/$date.$order.exon", $debug);


        $best_pos   = testPositions($posiTions, $gis[$b], $best_pos);
        $best_boj   = testBOJ($posBOJ, $gis[$b], $best_boj);
        my $whichGI = $gis[$b];
        $whichGI    = 'gi|'.$whichGI.'|'  if ( $whichGI =~ /^\d+$/ );
        print "\tNo exon-intron structure found for $whichGI with $fasta_header\n"  if ( %$posBOJ eq 0 );
    }

    # Remove temp files from exonerate and query sequence for exonerate
    unlink("$cache/$date.$order.fas", "$cache/$date.$order.exon");# if ($tmp == 0);


    return($best_pos, $best_boj);
}


sub testPositions{
    my ($posiTions, $currentTarget, $best_pos) = @_;

    my %positions = %$posiTions;
    if ( %positions ne 0 ){
        %positions = (%positions, '0' => $currentTarget);
        my @clefs  = sort({$a <=> $b} keys(%positions));
        foreach (@clefs) {
            print {*STDERR} "$_\t$positions{$_}\n" if ( $debug && $_>0 );
        }

        my @cles = sort({$a <=> $b} keys(%$best_pos));
        if ( !exists($cles[0]) ){
            return(\%positions);
        }
        else{
            return(\%positions) if ( exists($cles[0]) && $#cles < $#clefs );
            return($best_pos)   if ( exists($cles[0]) && $#cles >= $#clefs );
        }
    }
    elsif ( %$best_pos ne 0 ){
        return($best_pos);
    }
    else{
        return(\%positions);
    }
}

sub testBOJ{
    my ($posBOJ, $currentTarget, $bestBOJ) = @_;

    if ( $posBOJ && %$posBOJ ne 0 ){
        my %positBOJ = %$posBOJ;
        $positBOJ{'0'} = $currentTarget;

        my @clefs = sort({$a <=> $b} keys(%positBOJ));
        while(my ($x, $y) = each(%positBOJ) ){
            print {*STDERR} "pos:$x -> $y\n" if ( $debug && $x>0 );
        }

        my @cles = sort({$a <=> $b} keys(%$bestBOJ));
        if ( !exists($cles[0]) ){
            return(\%positBOJ);
        }
        else{
            return(\%positBOJ) if ( exists($cles[0]) && $#cles < $#clefs );
            return($bestBOJ)   if ( exists($cles[0]) && $#cles >= $#clefs );
        }
    }
    elsif ( $bestBOJ && %$bestBOJ ne 0 ){
        return($bestBOJ);
    }
    else {
        return($posBOJ);
    }
}
########################################################



################## Build output files ##################
sub prepareResults4CDS{
    my ($POS, $blastHit, $input_seq, $input_name) = @_;

    my %POSITIONS = %$POS;

    my $resultPOS = '';
    my ($bestOne, $annot) = ('', '');
    if ( %POSITIONS ne 0 ){
        ($bestOne, $annot) = prepareAnnotation($POSITIONS{0});
        my $seqName = buildAnnotation($bestOne, $annot, $input_name, $blastHit);
        my $seqCDS  = buildCDSseq($input_seq, %POSITIONS);
        $resultPOS  = '>'.$seqName."\n".$seqCDS."\n";
    }

    return($resultPOS, $bestOne);
}

sub prepareResults4BOJ{
    my ($BOJ, $blastHit, $input_seq, $input_name, $intronLess, $nameLess) = @_;

    my %EXONBORDERS = %$BOJ;

    my $resultBOJ = '';
    my ($bestOne, $annot) = ('', '');
    if ( %EXONBORDERS ne 0 ){
        ($bestOne, $annot) = prepareAnnotation($EXONBORDERS{0});
        my $seqName = buildAnnotation($bestOne, $annot, $input_name, $blastHit);
        my $seqBOJ  = buildBOJseq($input_seq, %EXONBORDERS);
        $resultBOJ  = '>'.$seqName."\n".$seqBOJ."\n";
    }
    elsif ( $intronLess ne '' ){
        ($bestOne, $annot) = prepareAnnotation($intronLess);
        my $description    = $annot;
        my $readyname      = '';
        $readyname         = fastaHeaders4ProtoGene($input_name);
        if ( $intronLess =~ /:.+/ || $bestOne =~ /^NA[CTWZGS]_/ || $nameLess =~ /^NA[CTWZGS]_/ ){
            if ( $nameLess eq '' ){
                $readyname =~ s/_G_@@/_G_$bestOne-IntronLess _S_ $blastHit _DESC_ /;
            }
            else {# $nameLess    ne ''
                $readyname =~ s/_G_@@/_G_$nameLess-IntronLess _S_ $blastHit _DESC_ /;
                $bestOne   = $nameLess;
            }

            $readyname =~ s/(.)$/$1 MATCHES_ON $description/ if ( $description ne '' );
        }
        else{
            $readyname =~ s/_G_@@/_G_Unavailable _S_ $blastHit _DESC_ /;
            $bestOne   = '';
        }
        $readyname =~ s/  +/ /g;
        $resultBOJ = '>'.$readyname."\n".$input_seq."\n";
    }

    return($resultBOJ, $bestOne);
}

sub prepareAnnotation{
    my ($best_pos) = @_;

    my $bestOne = $best_pos;
    $bestOne    =~ s{--.+$}{}  if ( $best_pos !~ /^\d+$/ );
    if ( $bestOne =~ /^\d+$/ ){
        open(my $BEST, '<', "$cache/$bestOne.fas");
        FIND_BEST:
        while(<$BEST>){
            if ($_ =~ /^>/){
                my $goodName = $_;
                chomp($goodName);
                $goodName    =~ s/^>//;
                $goodName    =~ s/^gi\|\d+\|//;
                $goodName    =~ s/^ *(\S+) *.*$/$1/;
                if ( $goodName =~ /^...?\|/ ){
                    $goodName    =~ s/^...?\|([\w\_\-]+(\.\d+)?).*/$1/;
                }
                $bestOne     = $goodName;
                last FIND_BEST;
            }
        }
        close $BEST;
    }
    my $annot = getFastaHeaderAnnot($best_pos);

    return($bestOne, $annot);
}

sub buildAnnotation{
    my ($bestOne, $annot, $input_name, $blastHit) = @_;

    $annot =~ s{>}{}g; #Remove > sign in annotation if any e.g.: see NM_105729
    my $readyName = '';
    $readyName    = fastaHeaders4ProtoGene($input_name);
    $readyName    =~ s/_G_@@/_G_$bestOne _S_ $blastHit _DESC_ /;
    $readyName    =~ s/(.)$/$1 MATCHES_ON $annot/ if ( $annot ne '' );
    $readyName    =~ s/  +/ /g;

    return($readyName);
}

sub buildCDSseq{
    my ($input_seq, %POSITIONS) = @_;

    my $cdsSeq = '';
    my $locus  = 0;
    for(my $z=0; $z<=length($input_seq); $z++){
        if ( $z==length($input_seq) ){
            if ( $input_seq =~ /\*$/ ){ # If * is in the original sequence no need to switch ---
                $cdsSeq =~ s{\-\-\-$}{};
            }
            $locus++;
            if ( exists($POSITIONS{$locus}) && ($POSITIONS{$locus} eq 'TAA' || $POSITIONS{$locus} eq 'TAG' || $POSITIONS{$locus} eq 'TGA') ){
                $cdsSeq .= $POSITIONS{$locus};
                #To add properly the stop codon, if any, at the end of the CDS
            }
            else{
                $cdsSeq .= '---'; #Else add 3x'-', if no stop codon were found, to keep the original MSA side length
            }
        }
        elsif ( substr($input_seq, $z, 1) =~ /[A-Za-z]/ ){
            $locus++;
            if ( exists($POSITIONS{$locus}) ){
                $cdsSeq .= $POSITIONS{$locus};
            }
#            elsif ( substr($input_seq, $z, 1) =~ /^[Mm]$/ ){ #for mismatches where we are sure of the triplet, i.e. monocodon Met et Trp
#                $cdsSeq .= 'ATG';
#            }
#            elsif ( substr($input_seq, $z, 1) =~ /^[Ww]$/ ){
#                $cdsSeq .= 'TGG';
#            }
            else{
                $cdsSeq .= 'NNN';
            }
        }
        else{
            $cdsSeq .= '---';
        }
    }

    return($cdsSeq);
}

sub buildBOJseq{
    my ($input_seq, %EXONBORDERS) = @_;

    my $bojSeq        = '';
    my $countNoGapPos = 0;
    for(my $as=0;$as < length($input_seq); $as++){
        my $whatIs = substr($input_seq, $as, 1);
        if ( $whatIs eq '-' ){
            $bojSeq .= $whatIs;
        }
        else{
            $countNoGapPos++;
            $bojSeq .= lc($EXONBORDERS{$countNoGapPos}) if (  exists($EXONBORDERS{$countNoGapPos}) );
            $bojSeq .= uc($whatIs)                      if ( !exists($EXONBORDERS{$countNoGapPos}) );
        }
    }

    return($bojSeq);
}

sub createCDSOutputFile{
    my ($CDSresultat, $original_seq, $original_name) = @_;

    open(my $CDS,  '>>', "$originalMSA.cds");
    open(my $CDSP, '>>', "$originalMSA.cdsP") if ( $pep==1 );
    $CDSresultat =~ s/  +/ /g;
    $CDSresultat =~ s/MATCHES_ON for >/MATCHES_ON for /;

    if ( $pep==1 ){
        my $peptide = $original_seq;
        $peptide    =~ s/(.)/$1--/g;
        print {$CDSP} $CDSresultat, ">$original_name\n$peptide\n";
    }

    my $CDSreformated = '';
    my @allCDS = split(/>/, $CDSresultat);
    for(my $er=0; $er<=$#allCDS; $er++){
        my ($CDSreformatedName, $CDSreformatedSeq) = ($allCDS[$er], $allCDS[$er]);
        $CDSreformatedName =~ s/^([^\n]*\n).+\n$/>$1/;
        $CDSreformatedSeq  =~ s/^[^\n]*\n(.+\n)$/$1/;
        $CDSreformatedSeq  =~ s/([^\n]{60})/$1\n/g;
        $CDSreformated    .= $CDSreformatedName.$CDSreformatedSeq;
    }
    print {$CDS} $CDSreformated;

    close $CDS;
    close $CDSP if ( $pep==1 );
    return;
}

sub createBOJOutputFile{
    my ($BOJresultat) = @_;
    $BOJresultat =~ s/  +/ /g;

    open(my $BOJ, '>>', "$originalMSA.boj");
    print {$BOJ} $BOJresultat;
    close $BOJ;

    return;
}

sub buildIntronlessBOJOutputFile{
    my ($BLASTstatus, $NTstatus, $NTname, $original_name, $original_seq) = @_;

    my $annot     = getFastaHeaderAnnot($NTstatus);
    my $readyname = '';
    $readyname    = fastaHeaders4ProtoGene($original_name);
    $NTstatus     =~ s{:.+$}{};
    $readyname    =~ s{_G_@@}{_G_$NTstatus-IntronLess _S_ $BLASTstatus _DESC_ } if ( $NTname eq '' );
    $readyname    =~ s{_G_@@}{_G_$NTname-IntronLess _S_ $BLASTstatus _DESC_ }   if ( $NTname ne '' );
    $readyname    =~ s{(.)$}{$1 MATCHES_ON $annot}                              if ( $annot  ne '' );
    $readyname    =~ s{  +}{ }g;

    open(my $BOJ, '>>', "$originalMSA.boj");
    print {$BOJ} ">$readyname\n$original_seq\n";
    close $BOJ;

    return;
}

sub buildFailureOutputFiles{
    my ($order, $BLASTstatus, $NTstatus) = @_;

    if ( $BLASTstatus eq 'No_BLASTp_Result' || $NTstatus eq 'PUI_unavailable' || $NTstatus eq 'No_nt_link' || $NTstatus eq 'Alignment_failure' ){
        open(CDS,  '>>', "$originalMSA.cds");
        open(CDSP, '>>', "$originalMSA.cdsP") if ( $pep==1 );
        open(OUT,  '>>', "$originalMSA.out");
        open(BOJ,  '>>', "$originalMSA.boj");

        revtransBuilding($original_names[$order], $order, $BLASTstatus, $NTstatus);

        close CDS;
        close CDSP if ( $pep==1 );
        close OUT;
        close BOJ;
    }

    return;
}

sub buildFailureBOJOutputFile{
    my ($BLASTstatus, $NTstatus, $original_name, $original_seq) = @_;

    my $readyname = '';
    $readyname    = fastaHeaders4ProtoGene($original_name);
    $readyname    =~ s{_G_@@}{_G_$NTstatus _S_ $BLASTstatus _DESC_ };
    $readyname    =~ s{  +}{ }g;

    open( my $BOJ, '>>', "$originalMSA.boj");
    print {$BOJ} ">$readyname\n$original_seq\n";
    close $BOJ;

    return;
}

sub revtransBuilding{
    my ($oriname, $order, $BLASTstatus, $NTstatus) = @_;

    my $readyname = '';
    $readyname    = fastaHeaders4ProtoGene($oriname);
    $readyname    =~ s{_G_@@}{_G_$NTstatus _S_ $BLASTstatus _DESC_ };
    $readyname    =~ s{  +}{ }g;
    print OUT "$readyname\n";

    if ( $revtrans ){
        my $final_seq = '';
        for(my $w=0; $w < length($original_seq[$order]); $w++){
            my $aa     = substr($original_seq[$order], $w, 1);
            $final_seq = $final_seq.reverse_trad($aa);
        }
        $readyname           =~ s{_G_$NTstatus _S_ $BLASTstatus }{_G_revtrans };
        my $CDSreformatedSeq = $final_seq."---\n"; #FIXME For stop codon ???
        $CDSreformatedSeq    =~ s{([^\n]{60})}{$1\n}g;
        chomp $CDSreformatedSeq if ( $CDSreformatedSeq =~ /\n+$/ );
        print CDS "$readyname\n$CDSreformatedSeq";

        if ( $pep==1 ){
            my $peptide = $original_seq[$order];
            $peptide    =~ s{(.)}{$1--}g;
            print CDSP "$readyname\n$final_seq---\n$original_names[$order]\n$peptide\n";
        }
    }

    return;
}

########################################################



################## Check output files ##################
sub checkAndCleanStderrFiles{
    my ($ExonerateStderrFiles) = @_;

    my $body = '';
    if ( -s "$ExonerateStderrFiles" ){
        my %uniq;
        open(my $ERRSTD, '<', "$ExonerateStderrFiles");
        while(<$ERRSTD>){
            %uniq = (%uniq, "$_" => '')
                if ( $_ !~ /^$/ && $_ !~ /Exhaustively generating suboptimal alignments will be (VERY SLOW|very slow)/
                        && $_ !~ /Message: Exhaustive alignment of/ && $_ !~ /Missing calc_macro for Calc/ && $_ !~ /Warning zero length sequence/  && $_ !~ /lookup up hostname:/ );
        }
        close $ERRSTD;
        while( my ($a, $b) = each(%uniq) ){
            $body .= $a;
        }

        if ( $body ne '' && $userEMail =~ /^[\w\_\-\.]+@[\w\_\-\.]+\.[A-Za-z][A-Za-z][A-Za-z]*$/ ){
            my $msg = new Mail::Send Subject => "[Protogene: ${date}_Error]", To => "$userEMail";
            my $fh  = $msg->open;
            print $fh $body;
            $fh->close; # complete the message and send it
        }
    }

    unlink("$ExonerateStderrFiles") if ( $tmp == 0 || $body eq '' );
    return;
}

sub checkOutputFiles{
    unlink("$originalMSA.cds")       if ( -z "$originalMSA.cds" );
    unlink("$originalMSA.cdsP")      if ( -z "$originalMSA.cdsP" );
    unlink("$originalMSA.cdsP.html") if ( -z "$originalMSA.cdsP.html" );
    unlink("$originalMSA.out")       if ( -z "$originalMSA.out" );
    unlink("$originalMSA.boj")       if ( -z "$originalMSA.boj" || !-e "$originalMSA.cds" );

    return;
}
########################################################



####################### Template #######################
sub template_failure {
    print {*STDERR} "\tYour template file has NOT the right format. Every line must look like:\n";
    print {*STDERR} "\t>name1_G_NAcc ... or\n\t>name1 _S_ PAcc ...\n";
    print {*STDERR} "\tWith NAcc = identifier or GI (from NCBI) of the nucleotidic target of your query name1\n";
    print {*STDERR} "\tWith PAcc = identifier       (from NCBI) of the peptidic    target of your query name1\n";
    print {*STDERR} "\t\tname1 must NOT contain space, '\\s', character\n";
    print {*STDERR} "\t\tidentifiers must NOT have version number at the end (NCBI)\n";
    print {*STDERR} "\tYou can provide your own nucleotidic sequence, below corresponding template line, with NAcc=My_Seq\n\n";
    print {*STDERR} "\tE.g.: >CDK2_hs Human Cyclin-Dependent Kinase 2\n";
    print {*STDERR} "\t      MENFQKVEKIGEGTYGVVYKARNKLTGEVVALK....\n\n";
    print {*STDERR} "\t\ttemplate file should be\n";
    print {*STDERR} "\t      >CDK2_hs_G_NC_000012\n";
    print {*STDERR} "\t\tif you provide a nucleotidic target identifier\n";
    print {*STDERR} "\t      >CDK2_hs _S_ NP_001789\n";
    print {*STDERR} "\t\tif you provide a peptidic target identifier\n";
    print {*STDERR} "\t      >CDK2_hs_G_My_Seq\n";
    print {*STDERR} "\t      TTCCTTCTCAGGGATAACACTCTATTCATGTCACTCCATTCA...\n";
    print {*STDERR} "\t\tif you provide your own nucleotidic target sequence\n\n";

    return;
}

sub checkTemplate {
    my ($templateFile) = @_;

    if ( -s "$templateFile" ){
        my (%proteinTarget, %nucleotideTarget, %nucleotideSeq);
        my $fasta_checker = 0;
        my $realSeqName   = '';

        open(my $TEMPLATE, '<', "$templateFile");
        READ_TEMPLATE:
        while(<$TEMPLATE>){
            if ( $_ =~ /^>/ ){
                $realSeqName   = '';
                next READ_TEMPLATE if ( $_ !~ /_[GS]_ *[^ ]*/ );
                $fasta_checker++;

                my $name = $_;
                $name    =~ s{\r\n}{}g; # Remove  return lines from windows OS '^M'
                $name    =~ s{\r}{\n}g; # Replace return lines from Mac OS
                chomp($name);

                $realSeqName = $name;
                $realSeqName =~ s/^(>.*?)_[GS]_.*$/$1/;
                $realSeqName =~ s{\s}{}g;
                $realSeqName = 'wHaT' if ($realSeqName !~ /[\w\>]/);

                my $protTarget    = $name;
                $protTarget       =~ s/^.*_S_ *([^ ]+) .*$/$1/;
                $protTarget       = '' if ( $protTarget !~ /^[\w]+$/ || $protTarget eq 'No_BLASTp_Result' || $protTarget =~ /^My_own_seq$/ );
                $proteinTarget{$realSeqName} = $protTarget;

                my $nuclTarget    = $name;
                $nuclTarget       =~ s/^.*_G_ *([^ ]+) .*$/$1/;
                $nuclTarget       = '' if ( $nuclTarget !~ /^[\w]+$/ || $nuclTarget eq 'No_nt_link' || $nuclTarget =~ /Not_searched/ || $nuclTarget =~ /navailable$/ || $nuclTarget eq 'Alignment_failure' );
                $nucleotideTarget{$realSeqName} = $nuclTarget;
                $nucleotideSeq{$realSeqName}    = '';
            }
            elsif ( $_ !~ /^>/ && exists($nucleotideTarget{$realSeqName}) && $nucleotideTarget{$realSeqName} =~ /^My_Seq$/i ){
                my $sequence = $_;
                $sequence    =~ s{\s}{}g;
                $sequence    =~ s/[\.\-]//g; # for msa with '.' as gap
                $sequence    =~ s/[^A-Za-z\*]//g; # Remove all the non-gap or non-alphabetic characters from the seq
                chomp($sequence);
                $nucleotideSeq{$realSeqName} .= $sequence;
            }
        }
        close $TEMPLATE;

        if ( $fasta_checker == 0 ){
            failure();
            template_failure();
            exit 1;
        }
        return (\%nucleotideTarget, \%proteinTarget, \%nucleotideSeq);
    }

    return(undef, undef, undef);
}

