#!/bin/sh

protog=`which ProtoGene.pl`
webbla=`which webblast.pl`
# Attention car les param de webblast pour Expresso ne sont pas updates via
# le script d'installation de ProtoGene.
# ex: le path pour les PDB doit etre modif a la main !
runbla=`which runblast.pl`

echo 'history.txt'
read hist
echo 'Exonerate.pm'
read loci
echo 'CheckOutput.pm'
read output
echo 'installer.temp'
read instalTMP


version=`$protog --version | sed -n -e 's/^.*version *: //p'`
name=ProtoGene_installer-$version.sh


echo $protog $webbla $runbla $hist $loci $output $instalTMP
cat $instalTMP >  $name
echo ''        >> $name
echo '#@@@@@@@ DELIM1 @@@@@@@# ' >> $name

cat $protog    >> $name
echo '#@@@@@@@ DELIM2 @@@@@@@# ' >> $name

cat $webbla    >> $name
echo '#@@@@@@@ DELIM3 @@@@@@@# ' >> $name

cat $runbla    >> $name
echo '#@@@@@@@ DELIM4 @@@@@@@# ' >> $name

cat $loci      >> $name
echo '#@@@@@@@ DELIM5 @@@@@@@# ' >> $name

cat $output    >> $name
echo '#@@@@@@@ DELIM6 @@@@@@@# ' >> $name

cat $hist      >> $name

chmod 555 $name
exit 0

