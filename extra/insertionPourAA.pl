#!/usr/bin/env perl

use strict;
use warnings;
use diagnostics;

my $infile = $ARGV[0] or die "\n\tYou must specify a fasta file to process\n\n";
open(my $FILE, '<', "$infile") if ( -e "$infile" );

my $seq  = '';
my $flag = 0;
while(<$FILE>){
    if ( $_ =~ /^>/ ){
        print $_     if ( $flag==0 );
        print "\n$_" if ( $flag>0 );
        $flag++;
    }
    else {
        $seq = $_;
        chomp($seq);
        $seq =~ s/ //g;
        $seq =~ s/\d//g;
        $seq =~ s/(.)/_${1}_/g;
        print $seq;
    }
}
close $FILE;
print "\n";

exit 0;

